import cv2
import numpy as np

cap = cv2.VideoCapture(0)
_,fst_frame  = cap.read()

while True:
    _, frame  = cap.read()
    diff = cv2.absdiff(fst_frame, frame)

    cv2.imshow("Frame", frame)
    cv2.imshow("diff", diff)
    #cv2.imshow("fst", fst_frame)

    key = cv2.waitKey(1)
    if key == 27:
        break


cap.release()
cv2.destroyAllWindows()