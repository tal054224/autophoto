import numpy as np
import cv2
from skimage import io
import time

def getDominate(img):
    pixels = np.float32(img.reshape(-1, 3))
    n_colors = 5
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 200, .1)
    flags = cv2.KMEANS_RANDOM_CENTERS

    _, labels, palette = cv2.kmeans(pixels, n_colors, None, criteria, 10, flags)
    _, counts = np.unique(labels, return_counts=True)
    dominant = palette[np.argmax(counts)]
    return dominant

def calcAvg(roi):
    avg_color_per_row = np.average(roi, axis=0)
    avg_color = np.average(avg_color_per_row, axis=0)
    return(avg_color)

face_template = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

cap = cv2.VideoCapture(0)

avg = [0,0,0]
# arrFrames = []

# for i in range(7):
#     name = 'images/face_' + str((i+1)) + ".jpg"
#     arrFrames.append(cv2.imread(name, cv2.IMREAD_COLOR))
count = 0
while True:
#for frame in arrFrames:
    # cv2.imshow('img',frame)
    # time.sleep(5)
    # start = time.time()
    
    
    ret, frame = cap.read()

    grayFrame =  cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_template.detectMultiScale(grayFrame, 1.3, 5)
    #print(faces)
    if( type(faces) is not tuple):
        #print("face detected")
        for (x,y,w,h) in faces:
            # print("x,y: " + str(x) + ", " + str(y))
            # print("w,h: " + str(w) + ", " + str(h))
            newH = int(h/2)
            newW = int(w/2)
            
            
            #croped_face = frame[y:y+h, x:x+w]
            croped_face = frame[newH + y:y+h, newW + x:x+w]   

            print("Avg: ", calcAvg(croped_face))
           # print("dominate: ", getDominate(frame))
            cv2.rectangle(frame, (x,y) ,(x+w, h+y), (255,0,0), 2 )
            #cv2.rectangle(frame, (newW + x, newH + y) ,(x+w, h+y), (255,0,0), 2 )

   
    # stop = time.time()
    # print(stop-start)


    cv2.imshow('img',frame)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break


cap.release()
cv2.destroyAllWindows()
