import PySimpleGUI as sg
import cv2
import numpy as np
def main():
    sg.theme("LightGreen")

    # Define the window layout
    layout = [
        [sg.Text("OpenCV Demo", size=(60, 1), justification="center")],
        [sg.Image(filename="", key="-IMAGE-")],
        [sg.Button("Reset", size=(10,1))],
        [sg.Button("Exit", size=(10, 1))],
    ]

    # Create the window and show it without the plot
    window = sg.Window("OpenCV Integration", layout, location=(800, 400))

    cap = cv2.VideoCapture(0)

    while True:
        event, values = window.read(timeout=20)
        ret, frame = cap.read()
        if event == "Exit" or event == sg.WIN_CLOSED:
            break
        
        if event=="Reset":
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        


        imgbytes = cv2.imencode(".png", frame)[1].tobytes()
        window["-IMAGE-"].update(data=imgbytes)

    window.close()

if __name__ == '__main__':
    main()