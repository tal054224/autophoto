var allLines;
var seqNumber;


chrome.storage.local.get("seqNum", function(data){
  seqNumber = data.seqNum;
});

chrome.runtime.getPackageDirectoryEntry(function(root) {
    root.getFile("passwords.txt", {}, function(fileEntry) {
      fileEntry.file(function(file) {
        var reader = new FileReader();
        reader.onloadend = function(e) {
          // contents are in this.result
          let rawText = this.result;
          allLines = rawText.split('\n');
        };
        reader.readAsText(file);
      });
    });
  });
  
document.querySelector("#get-pass").addEventListener("click", function(){
  if(seqNumber <= 100){
    copyTextToClipboard(allLines[seqNumber]);
    alert("password copied to calipboard\n" + allLines[seqNumber]);
    seqNumber+=1;
    chrome.storage.local.set({seqNum:seqNumber}, function () {});
  }
  else{  
    alert("time for new gestures!");
    chrome.storage.local.set({seqNum:0}, function () {});
  }
});


function copyTextToClipboard(text) {
  var textArea = document.createElement("textarea");

  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;

  textArea.style.width = '2em';
  textArea.style.height = '2em';
  textArea.style.padding = 0;

  // Clean up any borders.
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';

  textArea.style.background = 'transparent';


  textArea.value = text;

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Copying text command was ' + msg);
  } catch (err) {
    console.log('Oops, unable to copy');
  }

  document.body.removeChild(textArea);
}

