import numpy as np
import cv2
import PIL
from skimage import io
from PIL import Image
import skimage
from matplotlib import pyplot as plt



img = cv2.imread('images/hand.png')

"""im = Image.open('images/hand.jpg')
w,h = im.size
colors = im.getcolors(w*h)"""

grayScale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
grayScale =np.asarray(grayScale).flatten()


# cv2.imshow('pic', grayScale)
# # cv2.waitKey(0)
# hist = np.histogram(grayScale, 20)
plt.hist(grayScale, 10)
plt.title("Grayscale Histogram")
plt.xlabel("Bins")
plt.ylabel("# of Pixels")
plt.xlim([0, 256])
plt.show()


"""
num_red_bins = 5
num_green_bins = 5
num_blue_bins = 5

# Define threshold per bin
thresh_red = 256 // num_red_bins
thresh_green = 256 // num_green_bins
thresh_blue = 256 // num_blue_bins

# Extract planes
red = img[..., 0]
green = img[..., 1]
blue = img[..., 2]

# Calculate bin number per location
bin_red = red // thresh_red
bin_green = green // thresh_green
bin_blue = blue // thresh_blue

# Calculate 1D bin locations
bins = num_red_bins * num_green_bins * bin_blue + num_green_bins * bin_red + bin_green

#np.digitize()

# Calculate histogram
#histo = np.bincount(bins, minlength=num_red_bins * num_green_bins * num_blue_bins)

out_img = np.dstack((thresh_red * bin_red, thresh_green * bin_green, thresh_blue * bin_blue))

cv2.imshow("out", out_img)
cv2.waitKey(0)

img = cv2.cvtColor(out_img, cv2.COLOR_BGR2RGB)
im_pil = Image.fromarray(img)

w,h = im_pil.size
colors = im_pil.getcolors(w*h)

def toHex(color):
    r,g,b = color
    return '#%02x%02x%02x' % (r,g,b)

for idx, c in enumerate(colors):
    plt.bar(idx, c[0], color=toHex(c[1]),edgecolor=toHex(c[1]))

plt.show()
"""
"""
#cropped = img[]
avg_color_per_row = np.average(img, axis=0)
avg_color = np.average(avg_color_per_row, axis=0)
#print(avg_color)

pixels = np.float32(img.reshape(-1, 3))

n_colors = 5
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 200, .1)
flags = cv2.KMEANS_RANDOM_CENTERS

_, labels, palette = cv2.kmeans(pixels, n_colors, None, criteria, 10, flags)
_, counts = np.unique(labels, return_counts=True)

dominant = palette[np.argmax(counts)]
print(dominant)

"""