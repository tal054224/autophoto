import copy
import statistics
import sys
from statistics import median

import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
from scipy.signal import find_peaks

import config
import fps
import maxPoints
import myHelper as helper
import webcamVideoStream as cam

#check contours for single frame
#threahhold of canny
#change physical lighting- not optimal
#add fps rate
#use vlc to record video database
#clustering
 


#cap = cv2.VideoCapture(0)

fps = fps.fps().start()
def main():
    
    args = config._parse_args()

    vidName = 'videos//video5.mp4'

    cap = cam.webcamVideoStream(src=vidName).start()


    frame = getHandPic(cap, args)

    coloredRoi = frame[args.roiFstRecPoint[config.Y_POS]:args.roiSecRecPoint[config.Y_POS], args.roiFstRecPoint[config.X_POS]:args.roiSecRecPoint[config.X_POS]]  

    grayScale, grayScaleColor = getHandGrayScaleColor(coloredRoi) 
    mask =  ((grayScale > grayScaleColor) & (grayScale< grayScaleColor + config.BIN_SIZE))
    
    coloredRoi[mask] = 0 * (coloredRoi[mask]* 0 +1)
    
    indices = np.where(coloredRoi != 0)

    points = unique_coordinates = list(set(list(zip(indices[0], indices[1]))))
    medPointList = np.median(points, [0])
    medPoint = (int(medPointList[0]), int(medPointList[1]))

    handColor = coloredRoi[medPoint]


    lowerSkin = handColor -  config.COLOR_RANGE 
    upperSkin = handColor + config.COLOR_RANGE


    cv2.imshow('b', coloredRoi)
    cv2.waitKey(0)


    lastDefectNum = [None] * config.WINDOW_SIZE
    code = [0] * 4
    lastDominate = -1
    codeIndex = 0
    defectArrIndex=0

    count=30

    cap = cam.webcamVideoStream(src=vidName).start()
    while True:
        frame = cap.read()
        if frame is not None:
             
            #cv2.putText(frame, "_   _   _   _",(20,60), cv2.FONT_HERSHEY_SIMPLEX,3, config.BLUE,1)

            coloredRoi = frame[args.roiFstRecPoint[config.Y_POS]:args.roiSecRecPoint[config.Y_POS], args.roiFstRecPoint[config.X_POS]:args.roiSecRecPoint[config.X_POS]]  
            mask = (coloredRoi > lowerSkin) & (coloredRoi < upperSkin)
            roi = cv2.inRange(coloredRoi, lowerSkin, upperSkin)
            #coloredRoi[~mask] = 0 * (coloredRoi[~mask] * 0 + 1)
            #_, roi =  cv2.threshold(skinRegionHSVMask,0,255,cv2.THRESH_BINARY)

            #grayScale = cv2.cvtColor(coloredRoi, cv2.COLOR_BGR2GRAY)
            #find_peaks_cwt(grayScale[:,:,0], np.arange(80, 250))



            cv2.rectangle(frame, args.roiFstRecPoint, args.roiSecRecPoint, config.BLUE,0)

            contours, hirarchy = cv2.findContours(roi,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            colored_hand = frame[args.roiFstRecPoint[config.Y_POS]:args.roiSecRecPoint[config.Y_POS], args.roiFstRecPoint[config.X_POS]:args.roiSecRecPoint[config.X_POS]]



            if contours:
                hand_contour = max(contours, key = cv2.contourArea).squeeze() #largest contour  
                     
                drawing = np.zeros(colored_hand.shape,np.uint8)
                cv2.drawContours(drawing, [hand_contour], 0, config.YELLOW, 2) 
                # cv2.circle(drawing, tuple(hand_contour[0]), 4, config.BLUE,-1)   
                # cv2.circle(drawing, tuple(hand_contour[10]), 4, config.BLUE,-1)   
                # cv2.circle(drawing, tuple(hand_contour[hand_contour.shape[0] - 1]), 4, config.RED,-1)  
                # 
                numOfFingerShowing = 0 
                if count > 30:
                    minPointsIndexes = find_peaks(hand_contour[:,1], prominence=30)[0]  
                    minPoints = hand_contour[minPointsIndexes,:]
                    
                    indexesOfEndMaxima = np.where(minPoints[:,1] > colored_hand.shape[0] - 20)
                    minPoints = np.delete(minPoints, indexesOfEndMaxima, 0)
                    
                    
                    maxPointsIndexes = find_peaks(-hand_contour[:, 1], prominence=50)[0]
                    maxPoints = hand_contour[maxPointsIndexes,:]
                    maxEndPoint = [maxPoints, hand_contour[0]]
                    maxPoints = np.vstack(maxEndPoint)

                    for point in minPoints:
                        cv2.circle(drawing, tuple(point), 4, [0,255,0],-1)
                        
                    for point in maxPoints:
                        cv2.circle(drawing, tuple(point), 4, config.RED,-1)
                    
                    
                    if(len(maxPoints) == 1 and
                            len(minPoints) == 1 and
                            distance(maxPoints[0], minPoints[0]) > 100):
                            numOfFingerShowing = 1
                    elif(len(minPoints) >= 1 ):
                        numOfFingerShowing = len(minPoints) + 1


                    
                    #plt.plot(hand_contour[:,1], markevery=minPointsIndexes, marker="o")
                    #plt.show()
                    plt.clf()
                    count=30
                count+=1

                if numOfFingerShowing > 5:
                    numOfFingerShowing = 5
                cv2.putText(drawing, str(numOfFingerShowing), (0, 50), cv2.FONT_HERSHEY_SIMPLEX,1, (255, 0, 0) , 2, cv2.LINE_AA)


                cv2.drawContours(colored_hand, contours, -1, config.BLUE, 3)



                hull = cv2.convexHull(hand_contour) #search for parameters
                noPointsHull = cv2.convexHull(hand_contour, returnPoints=False)
                #defects = cv2.convexityDefects(hand_contour, noPointsHull)  
                defects = None  
                #cv2.drawContours(drawing, [hull], 0, config.BLUE, 2)


                # if defects is not None:
                #     numOfDefects, defectFlag = getDefectsNum(defects, hand_contour, drawing)
                #     if numOfDefects > 0:
                #             numOfDefects += 1    
                #     elif defectFlag:
                #         numOfDefects = 1         
                #     cv2.putText(drawing, str(numOfDefects), (0, 50), cv2.FONT_HERSHEY_SIMPLEX,1, (255, 0, 0) , 2, cv2.LINE_AA)
                    
                #     lastDefectNum[defectArrIndex % config.WINDOW_SIZE] = numOfDefects
                #     defectArrIndex+=1
                #     if defectArrIndex >= config.WINDOW_SIZE:
                #         medianOfDefects = np.median(lastDefectNum) 
                #         currDominateDefects = np.argmax(np.bincount(lastDefectNum))
                #         if lastDominate != currDominateDefects and codeIndex < 4 and currDominateDefects <= 5 and currDominateDefects != medianOfDefects:
                #             code[codeIndex] = currDominateDefects
                #             codeIndex+=1 
                #         lastDominate = medianOfDefects

               
                # cv2.putText(frame, str(code), (20,60), cv2.FONT_HERSHEY_SIMPLEX,3, config.BLUE,1)
                # median of window (defects points)
                # in case of confusion - check that median is accurate - the majority of the window
                #feature - username is face, password is gesture

            
                cv2.imshow("convex", drawing)
            cv2.imshow("Frame", frame)

        cv2.imshow("Colored_hand", colored_hand)
        cv2.imshow("roi", roi)
        
        #cv2.imshow("Edges", edges)
        fps.update()
        key = cv2.waitKey(1)
        if key == config.ESC:
            break
    


    fps.stop()
    print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
    print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
    cv2.destroyAllWindows()
    cap.stop()
    return

def getDefectsNum(defects, hand_contour, drawing):
    defectsFlag = False
    numOfDefects = 0
    for i in range(defects.shape[0]):  # calculate the angle
        s, e, f, d = defects[i][0]
        start = tuple(hand_contour[s][0])
        end = tuple(hand_contour[e][0])
        far = tuple(hand_contour[f][0])
        endToStart = np.sqrt((end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2)
        startToFar = np.sqrt((far[0] - start[0]) ** 2 + (far[1] - start[1]) ** 2)
        endToFar = np.sqrt((end[0] - far[0]) ** 2 + (end[1] - far[1]) ** 2)
        angle = np.arccos((startToFar ** 2 + endToFar ** 2 - endToStart ** 2) / (2 * startToFar * endToFar))  #      cosine theorem
        
        m = (end[1] - start[1]) / (end[0] - start[0])
        if angle <= np.pi / 2.5: # angle less than 90 degree, treat as fingers
            # cv2.circle(drawing, far, 4, [0, 255 ,0], -1)
            # cv2.circle(drawing, start, 4, [0, 255,0 ], -1)
            # cv2.circle(drawing, end, 4, [0, 255,0 ], -1)

            validFingerLength = (endToFar > 80 and startToFar > 80)
            oneFingerValidLength = (endToFar > 80 or startToFar > 80)
            firstFingerBend = startToFar > endToFar * 2  
            secFingerBend = startToFar* 2 < endToFar
            isOneFingerBend = np.logical_xor(firstFingerBend, secFingerBend)
            if validFingerLength:
                # #flag false in start
                numOfDefects += 1
            elif (isOneFingerBend or m>0) and oneFingerValidLength:
                defectsFlag = True

            #cv2.circle(drawing, far, 4, [0, 0, 255], -1)

    
    return numOfDefects, defectsFlag

def getHandGrayScaleColor(coloredRoi):
    grayScale = cv2.cvtColor(coloredRoi, cv2.COLOR_BGR2GRAY)
    grayScaleVector =np.asarray(grayScale).flatten()

    y,x,_ = plt.hist(grayScaleVector, 10)
    plt.title("Grayscale Histogram")
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    plt.xlim([0, 256])
    yReplica = copy.deepcopy(y)
    yReplica.sort()
    yReplica = yReplica[::-1]
    secColorIndex = np.where(y==yReplica[1])[0][0] #second greatest value

    grayScaleColor = x[secColorIndex] #probably the grayScale color of hand

    #plt.show()
    plt.clf() 
    return grayScale, grayScaleColor

def getHandPic(cap, args):
    handColorExist = False
    while not handColorExist:
        frame = cap.read()
        if frame is not None:
            cv2.putText(frame, "please place hand in square, then press SPACE", (20,50), cv2.FONT_HERSHEY_COMPLEX, 1, config.YELLOW)
            cv2.rectangle(frame, args.roiFstRecPoint, args.roiSecRecPoint, config.BLUE,0)
            cv2.imshow('Frame', frame) 
        keyPressed  = cv2.waitKey(1)
        if keyPressed == config.SPACE:
            handColorExist = True
            break
    cv2.destroyAllWindows()
    return frame
    #cap.release()


def edgeDetection(roi, sigma=0.20):
    v = np.median(roi)

    lower = int(max(0, (1.0-sigma)*v))
    upper = int(max(0, (1.0+sigma)*v))
    return cv2.Canny(roi, lower, upper)


def getHandColor(roi):
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, config.CRITERIA_SEC, config.CRITERIA_THIRD)
    pixels = np.float32(roi.reshape(config.ROWS, config.COLUMNS))
    _, labels, palette = cv2.kmeans(pixels, config.NUM_COLORS, None, criteria, config.TTL, cv2.KMEANS_RANDOM_CENTERS)
    _, counts = np.unique(labels, return_counts=True)
    if isInWhiteRange(palette[np.argmax(counts)]):
        return palette[np.argmax(counts) - 1]
    return palette[np.argmax(counts)]


def isInWhiteRange(color):
    r,g,b=color
    if( (config.LOWER_WHITE<= r <= config.UPPER_WHITE) and
     ( config.LOWER_WHITE<= g <= config.UPPER_WHITE) and
      ( config.LOWER_WHITE<= r <= config.UPPER_WHITE)):
        return True
    return False



def toHex(color):
    r,g,b = color
    return '#%02x%02x%02x' % (r,g,b)

def distance(v1, v2):
    return np.sqrt(np.sum((v1 - v2) ** 2))   

if __name__ == '__main__':
    main()
