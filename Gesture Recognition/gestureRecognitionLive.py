import copy
import statistics
import sys
from statistics import median

import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
from scipy.signal import find_peaks

import config
import fps
import maxPoints
import myHelper as helper
import webcamVideoStream as cam

import PySimpleGUI as sg
import passwords

fps = fps.fps().start()
def main():
    
    args = config._parse_args()

    vidName = 0

    sg.theme("LightGreen")

    # Define the window layout
    layout = [
        [sg.Text("AutoPhoto", size=(60, 1), justification="center")],
        [sg.Button("Reset", size=(10,1))],
        [sg.Button("Exit", size=(10, 1))],
        [sg.Image(filename="", key="-IMAGE-")],
        
    ]
    window = sg.Window("OpenCV Integration", layout, location=(800, 400))

    cap = cam.webcamVideoStream(src=vidName).start()


    lastDefectNum = [None] * config.WINDOW_SIZE
    code = [0] * 4
    lastDominate = -1
    codeIndex = 0
    defectArrIndex=0
    isThereHandPic = False
    while True:
        event, values = window.read(timeout=2)
        frame = cap.read()
        if not isThereHandPic:
            frame = getHandPic(cap, args)

            coloredRoi = frame[args.roiFstRecPoint[config.Y_POS]:args.roiSecRecPoint[config.Y_POS], args.roiFstRecPoint[config.X_POS]:args.roiSecRecPoint[config.X_POS]]  

            grayScale, grayScaleColor = getHandGrayScaleColor(coloredRoi) 
            mask =  ((grayScale > grayScaleColor) & (grayScale< grayScaleColor + config.BIN_SIZE))
            
            coloredRoi[mask] = 0 * (coloredRoi[mask]* 0 +1)
            
            indices = np.where(coloredRoi != 0)

            points = unique_coordinates = list(set(list(zip(indices[0], indices[1]))))
            medPointList = np.median(points, [0])
            medPoint = (int(medPointList[0]), int(medPointList[1]))

            handColor = coloredRoi[medPoint]


            lowerSkin = handColor -  config.COLOR_RANGE 
            upperSkin = handColor + config.COLOR_RANGE
            isThereHandPic = True
            cv2.imshow('b', coloredRoi)
            cv2.waitKey(0)
        elif frame is not None:
            coloredRoi = frame[args.roiFstRecPoint[config.Y_POS]:args.roiSecRecPoint[config.Y_POS], args.roiFstRecPoint[config.X_POS]:args.roiSecRecPoint[config.X_POS]]  
            mask = (coloredRoi > lowerSkin) & (coloredRoi < upperSkin)
            roi = cv2.inRange(coloredRoi, lowerSkin, upperSkin)



            cv2.rectangle(frame, args.roiFstRecPoint, args.roiSecRecPoint, config.BLUE,0)

            contours, hirarchy = cv2.findContours(roi,cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            colored_hand = frame[args.roiFstRecPoint[config.Y_POS]+2:args.roiSecRecPoint[config.Y_POS]-2, args.roiFstRecPoint[config.X_POS]+2:args.roiSecRecPoint[config.X_POS]-2]



            if contours:
                hand_contour = max(contours, key = cv2.contourArea).squeeze() #largest contour  
                     
                drawing = np.zeros(colored_hand.shape,np.uint8)
                cv2.drawContours(drawing, [hand_contour], 0, config.YELLOW, 2) 
                numOfFingerShowing = 0 

                minPoints, maxPoints = getPeakPoints(hand_contour, colored_hand)
                
                for point in minPoints:
                    cv2.circle(drawing, tuple(point), 4, [0,255,0],-1)
                    
                for point in maxPoints:
                    cv2.circle(drawing, tuple(point), 4, config.RED,-1)
                
                
                if(len(maxPoints) == 1 and
                        len(minPoints) == 1 and
                        distance(maxPoints[0], minPoints[0]) > 100):
                        numOfFingerShowing = 1
                elif(len(minPoints) >= 1 ):
                    numOfFingerShowing = len(minPoints) + 1


                if numOfFingerShowing > 5:
                    numOfFingerShowing = 5
                cv2.putText(drawing, str(numOfFingerShowing), (0, 50), cv2.FONT_HERSHEY_SIMPLEX,1, (255, 0, 0) , 2, cv2.LINE_AA)
                

                cv2.drawContours(colored_hand, contours, -1, config.BLUE, 3)

 
                lastDefectNum[defectArrIndex % config.WINDOW_SIZE] = numOfFingerShowing
                defectArrIndex+=1
                if defectArrIndex >= config.WINDOW_SIZE:
                    mostFrequentNum = np.argmax(np.bincount(lastDefectNum))
                    
                    if lastDominate != numOfFingerShowing and codeIndex < 4 and mostFrequentNum == numOfFingerShowing:
                        code[codeIndex] = numOfFingerShowing
                        codeIndex+=1 
                        lastDominate = numOfFingerShowing

                cv2.imshow("convex", drawing)
            cv2.putText(frame, str(code), (20,60), cv2.FONT_HERSHEY_SIMPLEX,3, config.BLUE,1)

                
            imgbytes = cv2.imencode(".png", frame)[1].tobytes()
            window["-IMAGE-"].update(data=imgbytes)
                
        if event == "Exit" or event == sg.WIN_CLOSED:
            break
        if event=="Reset":
            isThereHandPic=False
                
        #     cv2.imshow("Frame", frame)

        # cv2.imshow("Colored_hand", colored_hand)
        # cv2.imshow("roi", roi)
        


    fps.stop()
    print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
    print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))
    passwords.generateCodes(code)
    cv2.destroyAllWindows()
    cap.stop()
    
    return


def getPeakPoints(hand_contour, colored_hand):
    minPointsIndexes = find_peaks(hand_contour[:,1], prominence=20)[0]  
    minPoints = hand_contour[minPointsIndexes,:]
    
    indexesOfEndMaxima = np.where(minPoints[:,1] > colored_hand.shape[0] - 20)
    minPoints = np.delete(minPoints, indexesOfEndMaxima, 0)
    
    
    maxPointsIndexes = find_peaks(-hand_contour[:, 1], prominence=50)[0]
    maxPoints = hand_contour[maxPointsIndexes,:]
    maxEndPoint = [maxPoints, hand_contour[0]]
    maxPoints = np.vstack(maxEndPoint)


    return minPoints, maxPoints


def getHandGrayScaleColor(coloredRoi):
    grayScale = cv2.cvtColor(coloredRoi, cv2.COLOR_BGR2GRAY)
    grayScaleVector =np.asarray(grayScale).flatten()

    y,x,_ = plt.hist(grayScaleVector, 10)
    plt.title("Grayscale Histogram")
    plt.xlabel("Bins")
    plt.ylabel("# of Pixels")
    plt.xlim([0, 256])
    yReplica = copy.deepcopy(y)
    yReplica.sort()
    yReplica = yReplica[::-1]
    secColorIndex = np.where(y==yReplica[1])[0][0] #second greatest value

    grayScaleColor = x[secColorIndex] #probably the grayScale color of hand
    plt.clf() 
    return grayScale, grayScaleColor

def getHandPic(cap, args):
    handColorExist = False
    while not handColorExist:
        frame = cap.read()
        if frame is not None:
            cv2.putText(frame, "please place hand in square, then press SPACE", (20,50), cv2.FONT_HERSHEY_COMPLEX, 1, config.YELLOW)
            cv2.rectangle(frame, args.roiFstRecPoint, args.roiSecRecPoint, config.BLUE,0)
            cv2.imshow('Frame', frame) 
        keyPressed  = cv2.waitKey(1)
        if keyPressed == config.SPACE:
            handColorExist = True
            break
    cv2.destroyAllWindows()
    return frame
    #cap.release()


def distance(v1, v2):
    return np.sqrt(np.sum((v1 - v2) ** 2))   


if __name__ == '__main__':
    main()
