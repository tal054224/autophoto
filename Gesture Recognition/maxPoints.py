from statistics import median
import matplotlib.pyplot as plt
import config
from math import sqrt

SQUARED = 2

class maxPoints():
    def __init__(self, allPoints):
        self.allPoints = allPoints
        self.allX = self.getAllX()
        self.allY = self.getAllY()
        notInt = self.getMedian()
        self.median = (int(notInt[config.X_POS]), int(notInt[config.Y_POS]))

    def getMedian(self):
        return (median(self.allX), median(self.allY))

    def getAllX(self):
        if len(self.allPoints):
            return [i[config.X_POS] for i in self.allPoints]
        return None

    def getAllY(self):
        if len(self.allPoints):
            return [i[config.Y_POS] for i in self.allPoints]
        return None

    def showGraph(self):
        for point in self.allPoints:
            plt.scatter(point[config.X_POS], point[config.Y_POS],c=['#343aeb'])
        plt.scatter(self.median[config.X_POS], self.median[config.Y_POS],c=['#f00a0a'])    
        plt.show()

    def getClosestPoint(self):
        closestPoint = self.allPoints[0]
        minDist = self.dist(closestPoint, self.median)
        for p in self.allPoints:
            currDist = self.dist(p, self.median)
            if currDist < minDist:
                minDist = currDist
                closestPoint = p
        
        return closestPoint, self.allPoints.index(closestPoint)
        
    def dist(self, p1,p2):
        return abs(sqrt((p2[config.X_POS]-p1[config.X_POS])**SQUARED + (p2[config.Y_POS]-p1[config.Y_POS])**SQUARED))



