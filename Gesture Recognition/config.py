#config imports
import argparse
import sys
import os


X_POS = 0
Y_POS = 1

YELLOW = (0, 255, 255)
RED = (0,0,255)
BLUE = (255,0,0)
GREEN = (0,255,0)

LEFT_POS = 0
RIGHT_POS = 1
TOP_POS = 2
BOTTOM_POS = 3

SPACE = 32
ESC = 27

NUM_COLORS=100

ROWS=-1
COLUMNS=3
CRITERIA_SEC=200
CRITERIA_THIRD=.1
TTL=10

LOWER_WHITE=200
UPPER_WHITE=255

COLOR_RANGE=50

NUM_RED_BINS=8
NUM_GREEN_BINS=8
NUM_BLUE_BINS=8

THRESH_RED=256//NUM_RED_BINS
THRESH_GREEN=256//NUM_GREEN_BINS
THRESH_BLUE=256//NUM_BLUE_BINS


BIN_SIZE = 30

WINDOW_SIZE =30

def _parse_args():

    ap = argparse.ArgumentParser(description='AutoPhoto Config')

    #essential
    ap.add_argument('--edgeMinThreshold', type=int, help='Minimum threshold of cv2.Canny', default=60)
    ap.add_argument('--edgeMaxThreshold', type=int, help='Maximum threshold of cv2.Canny', default=120)

    #temp

    #(550,100)
    #(1100,460)

    #(0,100)
    #(400,460)

    #(80, 80)
    #(400, 400)360, 450
    ap.add_argument('--roiFstRecPoint', type=tuple, help='The first point of roi rectangle (x,y)', default=(30, 100))
    ap.add_argument('--roiSecRecPoint', type=tuple, help='The second point of roi rectangle (x,y)', default=(460, 550))
    
    #good to have
    

    return ap.parse_args()
