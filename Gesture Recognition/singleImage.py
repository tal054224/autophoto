import cv2
import numpy as np
import config
args = config._parse_args()
while True:

    img = cv2.imread('images/hand.jpeg')

    imgray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if contours:
        hand_contour = max(contours, key = cv2.contourArea)
        cv2.drawContours(img, [hand_contour], 0,config.BLUE, 2)


        
    cv2.imshow('img', img)


    key = cv2.waitKey(0)
    if key == 27:
        break
cv2.destroyAllWindows()