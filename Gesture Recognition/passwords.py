from random import choice,getrandbits
import hashlib
import pyDes
def generateCodes(code):
    with open('./GUI/passwords.txt', 'a') as file:
        file.write("1\n")
        for i in range(100):
            randomSequence = [choice(code) for i in range(24)]
            randomSequenceStr = "".join(str(x) for x in randomSequence)
            encrypted = pyDes.triple_des(randomSequenceStr).encrypt(str(randomSequence), padmode=2)
            hashCode = hashlib.md5(encrypted)
            file.write(hashCode.hexdigest())
            file.write("\n")
    
generateCodes([5,1,3,2])
