import cv2
import numpy as np
from matplotlib import pyplot as plt
from math import sqrt
#argwhere



def findExtremePoints(hand):
    #extreme points (finds the min/max values of x or y)
    extLeft = tuple(hand[hand[:, :, 0].argmin()][0])
    extRight = tuple(hand[hand[:, :, 0].argmax()][0])
    extTop = tuple(hand[hand[:, :, 1].argmin()][0])
    extBot = tuple(hand[hand[:, :, 1].argmax()][0])
    return extLeft, extRight, extTop, extBot

def showConvexes(convexes):
    cnt = 0
    x = 0
    y = 0
    for i in convexes:
        cv2.imshow("i" + str(cnt), i)
        cv2.moveWindow("i" + str(cnt),x , y)
        cnt+=1
        x+=220
        if x>= 1300:
            x=0
            y+=220
 
    while True:
        key = cv2.waitKey(1)
        if key == 27:
            break

def toHex(color):
    r,g,b = color
    return '#%02x%02x%02x' % (r,g,b)

def colorDistance(fstColor, secColor):
    r1,g1,b1 = fstColor
    r2,g2,b2 = secColor
    rmean = (r1 + r2)/2
    r = r1-r2
    g = g1-g2
    b = b1-b2
    return sqrt((int(((512+rmean)*r*r))>>8) +4*g*g+(int(((767-rmean)*b*b))>>8))


def toHex(color):
    r,g,b = color
    return '#%02x%02x%02x' % (r,g,b)